provider "aws" {
    region = "us-east-2"
}
terraform {
    required_version = "v0.12.21"
}
resource "aws_db_instance" "example"{
    identifier_prefix   = "terraform-up-and-running-${var.environment}"
    engine              = "mysql"
    allocated_storage   = 10
    instance_class      = "db.t2.micro"
    name                = var.db_name
    username            = var.db_username
    password            = var.db_password
    skip_final_snapshot = true
}