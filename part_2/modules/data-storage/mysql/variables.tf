variable "db_password"{
    description = "The password for the database"
    type = string
}
variable "environment" {
  description = "The name of the environment we're deploying to"
  type        = string
}
variable "db_name" {
    description = "The name for the database"
    type = string
}
variable "db_username" {
    description = "The name of user for the database"
    type = string
    default = "admin"
}