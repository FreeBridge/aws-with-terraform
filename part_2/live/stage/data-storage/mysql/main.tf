provider "aws" {
    region = "us-east-2"
}
terraform {
    required_version = "v0.12.21"

    backend "s3"{
        key = "stage/data-storage/mysql/terraform.tfstate"
    }
}
module "mysql" {
    source = "../../../../modules/data-storage/mysql"
    
    environment = "mysql-stage"
    db_password = var.db_password
    db_name     = "example-stage-database"
    db_username = "admin"
}