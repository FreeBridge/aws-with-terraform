# outputs
#output "public_ip" {
#    value = module.webserver_cluster.public_ip
#    description = "The public ip of web server"
#}
output "alb_dns_name" {
    value = module.webserver_cluster.alb_dns_name
    description = "The domain name of the load balancer"
}