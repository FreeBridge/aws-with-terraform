provider "aws" {
    region = "us-east-2"
}
module "webserver_cluster" {
    source = "../../../../modules/services/hello-world-app"

    ami = "ami-0c55b159cbfafe1f0"
    server_text = "New server text"

    environment             = "webservers-stage"
    db_remote_state_bucket  = "my-first-test-with-s3"
    db_remote_state_key     = "stage/data-storage/mysql/terraform.tfstate"

    instance_type           = "t2.micro"
    min_size                = 2
    max_size                = 2
    enable_autoscaling      = false
#    enable_new_user_data    = true
}