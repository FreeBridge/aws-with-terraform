provider "aws" {
    region = "us-east-2"
}

terraform {
  backend "s3"{
      key = "global/s3/terraform.tfstate"
  }
}

resource "aws_s3_bucket" "terraform_state" {
    bucket = "my-first-test-with-s3"
    
    # disable random delete this bucket
    lifecycle {
        prevent_destroy = true
    }

    # enable versioning
    versioning {
        enabled = true
    }

    # enable servers encryption
    server_side_encryption_configuration {
        rule {
            apply_server_side_encryption_by_default{
                sse_algorithm = "AES256"
            }
        }
    }
}

resource "aws_dynamodb_table" "terraform_locks" {
    name = "my-first-locks"
    billing_mode = "PAY_PER_REQUEST"
    hash_key = "LockID"

    attribute {
      name = "LockID"
      type = "S"
    }
}