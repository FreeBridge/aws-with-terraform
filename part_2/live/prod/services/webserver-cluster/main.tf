provider "aws" {
    region = "us-east-2"
}
module "webserver_cluster" {
    source = "../../../../modules/services/hello-world-app"

    environment            = "webservers-prod"
    db_remote_state_bucket  = "my-first-test-with-s3"
    db_remote_state_key     = "prod/data-storage/mysql/terraform.tfstate"

    instance_type           = "t2.micro"
    min_size                = 2
    max_size                = 4
    enable_autoscaling      = true
#    enable_new_user_data    = false

    custom_tags = {
        Owner       = "my-team"
        DeployedBy  = "terraform"
    }
}