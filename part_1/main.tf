provider "aws" {
    region = "us-east-2"
}

resource "aws_instance" "example" {
    # Ubuntu 18.04
    ami     =   "ami-0c55b159cbfafe1f0"
    instance_type = "t2.micro"
    # for use
    vpc_security_group_ids = [aws_security_group.instance.id]
    # added script in my web-server
    user_data = <<-EOF
                #!/bin/bash
                echo "Hello, World" > index.html
                nohup busybox httpd -f -p ${var.server_port} &
                EOF

    tags = {
        Name = "terraform-example"
    }
}

# create security group for web-server
resource "aws_security_group" "instance" {
    name    = "terraform-example-instance"

    ingress{
        from_port = var.server_port
        to_port   = var.server_port
        protocol  = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_launch_configuration" "example" {
    image_id = "ami-0c55b159cbfafe1f0"
    instance_type = "t2.micro"
    # for use
    security_groups = [aws_security_group.instance.id]
    # added script in my web-server
    user_data = <<-EOF
                #!/bin/bash
                echo "Hello, World" > index.html
                nohup busybox httpd -f -p ${var.server_port} &
                EOF
}
resource "aws_autoscaling_group" "example" {
    launch_configuration = aws_launch_configuration.example.name
    vpc_zone_identifier = data.aws_subnet_ids.default.ids

    target_group_arns = [ aws_lb_target_group.asg.arn ]
    health_check_type = "ELB"

    min_size = 2
    max_size = 10

    tag{
        key = "Name"
        value = "terraform-asg-example"
        propagate_at_launch = true
    }

    lifecycle {
      create_before_destroy = true
    }
  
}

resource "aws_lb" "example" {
    name = "terraform-asg-example"
    load_balancer_type = "application"
    subnets = data.aws_subnet_ids.default.ids
    security_groups = [ aws_security_group.alb.id ]
}

resource "aws_lb_listener" "http" {
    load_balancer_arn = aws_lb.example.arn
    port = 80
    protocol = "HTTP"

    default_action{
        type = "fixed-response"

        fixed_response{
            content_type = "text/plain"
            message_body = "404: page not found"
            status_code = 404
        }
    }
}

resource "aws_lb_listener_rule" "asg" {
    listener_arn = aws_lb_listener.http.arn
    priority = 100

    condition{
        path_pattern {
            values = ["*"]
        }
    }

    action{
        type = "forward"
        target_group_arn = aws_lb_target_group.asg.arn
    }
}

resource "aws_security_group" "alb" {
    name = "terraform-example-alb"

    # Enable all incoming HTTP-request
    ingress {
      from_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      to_port = 80
    }

    # Enable all outgoing HTTP-request
    egress{
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_lb_target_group" "asg" {
    name = "terraform-asg-example"
    port = var.server_port
    protocol = "HTTP"
    vpc_id = data.aws_vpc.default.id

    health_check{
        path = "/"
        protocol = "HTTP"
        matcher = "200"
        interval = 15
        timeout = 3
        healthy_threshold = 2
        unhealthy_threshold = 2
    }
}

# data
data "aws_vpc" "default"{
    default = true
}
data "aws_subnet_ids" "default" {
    vpc_id = data.aws_vpc.default.id
}
# outputs
output "public_ip" {
    value = aws_instance.example.public_ip
    description = "The public ip of web server"
  
}
output "alb_dns_name" {
    value = aws_lb.example.dns_name
    description = "The domain name of the load balancer"
}
# variables
variable "server_port" {
    description = "The port the server will use for HTTP requests"
    type        = number
    default = 8080
}